package storage

import (
	p "path"

	"io"
	"os"
)

type fsStore struct {
	path string
}

func (st *fsStore) Create(id string, name string) (io.WriteCloser, error) {
	path := idPath(st.path, id)
	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		return nil, err
	}

	return os.Create(p.Join(path, name))
}

func (st *fsStore) Store(id string, file io.Reader, name string) (size int64, err error) {
	dest, err := st.Create(id, name)
	if err != nil {
		return 0, err
	}
	defer dest.Close()

	return io.Copy(dest, file)
}

func (st *fsStore) Get(id string, name string) (File, error) {
	path := idPath(st.path, id)
	return os.Open(p.Join(path, name))
}

func (st *fsStore) Delete(id string) error {
	path := idPath(st.path, id)
	return os.RemoveAll(path)
}
