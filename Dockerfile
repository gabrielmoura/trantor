FROM golang as builder

WORKDIR /trantor/
COPY main.go go.* /trantor/
COPY lib/ /trantor/lib/

RUN CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' .

FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /trantor/trantor /trantor

COPY robots.txt description.json opensearch.xml key.asc /assets/
COPY img /assets/img/
COPY css /assets/css/
COPY js /assets/js/
COPY templates /assets/templates/
COPY logger.xml /

ENV TRANTOR_ASSETS=/assets
ENV TRANTOR_STORE=/store
VOLUME /store /tmp
WORKDIR /

ENTRYPOINT ["/trantor"]
